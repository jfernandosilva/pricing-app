package com.app.pricing.infrastructure.outbound.database.repository;

import com.app.pricing.infrastructure.outbound.database.entity.PriceEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class PriceEntityJpaRepositoryIntegrationTest {
    private static final Long BRAND_ID = 1L;
    private static final Long PRODUCT_ID = 35455L;

    private final PriceEntityJpaRepository priceEntityJpaRepository;

    @Autowired
    public PriceEntityJpaRepositoryIntegrationTest(PriceEntityJpaRepository priceEntityJpaRepository) {
        this.priceEntityJpaRepository = priceEntityJpaRepository;
    }

    @Test
    public void getAllByBrandIdAndProductIdAndDateTimeWithZeroRecords() {
        LocalDateTime dateTime = LocalDateTime.of(2000, 06, 14, 00, 00, 00);
        List<PriceEntity> prices = priceEntityJpaRepository.getAllByBrandIdAndProductIdAndDateTime(BRAND_ID, PRODUCT_ID, dateTime);
        assertTrue(prices.size() == 0);
    }

    @Test
    public void getAllByBrandIdAndProductIdAndDateTimeWithOneRecord() {
        LocalDateTime dateTime = LocalDateTime.of(2020, 06, 14, 18, 40, 00);
        List<PriceEntity> prices = priceEntityJpaRepository.getAllByBrandIdAndProductIdAndDateTime(BRAND_ID, PRODUCT_ID, dateTime);
        assertTrue(prices.size() == 1);
        assertEquals(1L, prices.get(0).getPriceList());
        assertEquals(0, prices.get(0).getPriority());
        assertEquals(new BigDecimal("35.50"), prices.get(0).getPrice());
    }

    @Test
    public void getAllByBrandIdAndProductIdAndDateTimeWithMoreOneRecord() {
        LocalDateTime dateTime = LocalDateTime.of(2020, 12, 25, 18, 40, 00);
        List<PriceEntity> prices = priceEntityJpaRepository.getAllByBrandIdAndProductIdAndDateTime(BRAND_ID, PRODUCT_ID, dateTime);
        assertTrue(prices.size() == 2);
        assertEquals(1L, prices.get(0).getPriceList());
        assertEquals(0, prices.get(0).getPriority());
        assertEquals(new BigDecimal("35.50"), prices.get(0).getPrice());
        assertEquals(4L, prices.get(1).getPriceList());
        assertEquals(1, prices.get(1).getPriority());
        assertEquals(new BigDecimal("38.95"), prices.get(1).getPrice());
    }
}