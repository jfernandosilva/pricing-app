package com.app.pricing.infrastructure.outbound.database;

import com.app.pricing.infrastructure.outbound.database.repository.PriceEntityJpaRepository;
import com.app.pricing.infrastructure.outbound.database.repository.PriceRepositoryImpl;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.mockito.Mockito.*;


class PriceRepositoryImplTest {
    @Test
    public void whenGetAllByBrandIdAndProductIdAndDateTimeThenUseJpaRepositoryToObtainData() {
        PriceEntityJpaRepository priceEntityJpaRepository = mock(PriceEntityJpaRepository.class);
        PriceRepositoryImpl priceRepository = new PriceRepositoryImpl(priceEntityJpaRepository);

        Long brandId = 10L;
        Long productId = 20L;
        LocalDateTime dateTime = LocalDateTime.now();
        priceRepository.getAllByBrandIdAndProductIdAndDateTime(brandId, productId, dateTime);

        verify(priceEntityJpaRepository, times(1)).getAllByBrandIdAndProductIdAndDateTime(eq(brandId), eq(productId), eq(dateTime));
    }

}