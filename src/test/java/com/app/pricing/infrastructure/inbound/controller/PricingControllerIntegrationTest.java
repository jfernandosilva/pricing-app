package com.app.pricing.infrastructure.inbound.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class PricingControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void whenPriceDoesNotExistsItReturnsANotFoundCode() throws Exception {
        this.mockMvc
                .perform(get("/prices")
                        .param("date", "1999-06-16 21:00:00")
                        .param("brand_id", "1")
                        .param("product_id", "35455")
                )
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.code").value("03"))
                .andExpect(jsonPath("$.message").value("PRICE NOT FOUND WITH GIVEN PARAMETERS"));
    }

    @Test
    void whenRequiredParametersAreNotPresentsItReturnsABadRequestError() throws Exception {
        this.mockMvc
                .perform(get("/prices")
                        .param("brand_id", "1")
                        .param("product_id", "35455")
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.code").value("01"))
                .andExpect(jsonPath("$.message").value("The request does not have the required parameters"));

        this.mockMvc
                .perform(get("/prices")
                        .param("date", "1999-06-16 21:00:00")
                        .param("product_id", "35455")
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.code").value("01"))
                .andExpect(jsonPath("$.message").value("The request does not have the required parameters"));

        this.mockMvc
                .perform(get("/prices")
                        .param("date", "1999-06-16 21:00:00")
                        .param("brand_id", "1")
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.code").value("01"))
                .andExpect(jsonPath("$.message").value("The request does not have the required parameters"));
    }

    @Test
    void whenParameterHasWrongTypeItRetursABadRequestError() throws Exception {
        this.mockMvc
                .perform(get("/prices")
                        .param("date", "10")
                        .param("brand_id", "1")
                        .param("product_id", "35455")
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.code").value("02"))
                .andExpect(jsonPath("$.message").value("The request has parameters with wrong type"));


        this.mockMvc
                .perform(get("/prices")
                        .param("date", "2020-06-14 10:00:00")
                        .param("brand_id", "x")
                        .param("product_id", "35455")
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.code").value("02"))
                .andExpect(jsonPath("$.message").value("The request has parameters with wrong type"));

        this.mockMvc
                .perform(get("/prices")
                        .param("date", "2020-06-14 10:00:00")
                        .param("brand_id", "1")
                        .param("product_id", "A")
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.code").value("02"))
                .andExpect(jsonPath("$.message").value("The request has parameters with wrong type"));
    }


    @Test
    @DisplayName("Test price of 2020-06-14 10:00:00")
    void test1() throws Exception {
        this.mockMvc
                .perform(get("/prices")
                        .param("date", "2020-06-14 10:00:00")
                        .param("brand_id", "1")
                        .param("product_id", "35455")
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.brand_id").value(1))
                .andExpect(jsonPath("$.price_list_id").value(1))
                .andExpect(jsonPath("$.price").value("35.50 EUR"));

    }

    @Test
    @DisplayName("Test price of 2020-06-14 16:00:00")
    void test2() throws Exception {
        this.mockMvc
                .perform(get("/prices")
                        .param("date", "2020-06-14 16:00:00")
                        .param("brand_id", "1")
                        .param("product_id", "35455")
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.brand_id").value(1))
                .andExpect(jsonPath("$.price_list_id").value(2))
                .andExpect(jsonPath("$.price").value("25.45 EUR"));

    }

    @Test
    @DisplayName("Test price of 2020-06-14 21:00:00")
    void test3() throws Exception {
        this.mockMvc
                .perform(get("/prices")
                        .param("date", "2020-06-14 21:00:00")
                        .param("brand_id", "1")
                        .param("product_id", "35455")
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.brand_id").value(1))
                .andExpect(jsonPath("$.price_list_id").value(1))
                .andExpect(jsonPath("$.price").value("35.50 EUR"));

    }

    @Test
    @DisplayName("Test price of 2020-06-15 10:00:00")
    void test4() throws Exception {
        this.mockMvc
                .perform(get("/prices")
                        .param("date", "2020-06-15 10:00:00")
                        .param("brand_id", "1")
                        .param("product_id", "35455")
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.brand_id").value(1))
                .andExpect(jsonPath("$.price_list_id").value(3))
                .andExpect(jsonPath("$.price").value("30.50 EUR"));

    }

    @Test
    @DisplayName("Test price of 2020-06-16 21:00:00")
    void test5() throws Exception {
        this.mockMvc
                .perform(get("/prices")
                        .param("date", "2020-06-16 21:00:00")
                        .param("brand_id", "1")
                        .param("product_id", "35455")
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.brand_id").value(1))
                .andExpect(jsonPath("$.price_list_id").value(4))
                .andExpect(jsonPath("$.price").value("38.95 EUR"));

    }
}