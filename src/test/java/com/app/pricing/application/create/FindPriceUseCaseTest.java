package com.app.pricing.application.create;

import com.app.pricing.domain.exception.PriceNotFoundException;
import com.app.pricing.domain.model.Price;
import com.app.pricing.domain.repository.PriceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FindPriceUseCaseTest {
    private static final Long BRAND_ID = 1L;
    private static final Long PRODUCT_ID = 35455L;
    private static final Price p1 = new Price(BRAND_ID, LocalDateTime.of(2020, 06, 14, 00, 00, 00), LocalDateTime.of(2020, 12, 31, 23, 59, 59), 1L, PRODUCT_ID, 0, new BigDecimal("35.50"), "EUR");
    private static final Price p2 = new Price(BRAND_ID, LocalDateTime.of(2020, 06, 14, 15, 00, 00), LocalDateTime.of(2020, 06, 14, 18, 30, 00), 2L, PRODUCT_ID, 1, new BigDecimal("25.45"), "EUR");
    private static final Price p3 = new Price(BRAND_ID, LocalDateTime.of(2020, 06, 15, 00, 00, 00), LocalDateTime.of(2020, 06, 15, 11, 00, 00), 3L, PRODUCT_ID, 1, new BigDecimal("30.50"), "EUR");
    private static final Price p4 = new Price(BRAND_ID, LocalDateTime.of(2020, 06, 15, 16, 00, 00), LocalDateTime.of(2020, 12, 31, 23, 59, 59), 4L, PRODUCT_ID, 1, new BigDecimal("38.95"), "EUR");

    private PriceRepository priceRepository;
    private FindPriceUseCase findPriceUseCase;

    @BeforeEach
    public void initEach() {
        priceRepository = mock(PriceRepository.class);
        findPriceUseCase = new FindPriceUseCase(priceRepository);
    }


    @Test
    public void whenDataRepositoryReturnsOneRecordForTargetParametersItReturnsThisSinglePrice() throws PriceNotFoundException {
        LocalDateTime dateTime = LocalDateTime.of(2020, 06, 15, 00, 00, 00);
        when(priceRepository.getAllByBrandIdAndProductIdAndDateTime(eq(BRAND_ID), eq(PRODUCT_ID), eq(dateTime))).thenReturn(Collections.singletonList(p1));
        Price price = findPriceUseCase.findByBrandIdAndProductIdAndDateTime(BRAND_ID, PRODUCT_ID, dateTime);
        assertEquals(new BigDecimal("35.50"), price.getPrice());
    }

    @Test
    public void whenDataRepositoryReturnsZeroRecordsForTargetParametersItReturnAPriceNotFoundException() throws PriceNotFoundException {
        LocalDateTime dateTime = LocalDateTime.of(2000, 06, 15, 00, 00, 00);
        when(priceRepository.getAllByBrandIdAndProductIdAndDateTime(eq(BRAND_ID), eq(PRODUCT_ID), eq(dateTime))).thenReturn(Collections.emptyList());

        assertThrows(PriceNotFoundException.class,
                () -> findPriceUseCase.findByBrandIdAndProductIdAndDateTime(BRAND_ID, PRODUCT_ID, dateTime));
    }

    @Test
    public void whenDataRepositoryReturnsMoreOneRecordForTargetParametersItReturnsPriceWithBiggestPriority() throws PriceNotFoundException {
        LocalDateTime dateTime = LocalDateTime.of(2020, 07, 01, 00, 00, 00);
        when(priceRepository.getAllByBrandIdAndProductIdAndDateTime(eq(BRAND_ID), eq(PRODUCT_ID), eq(dateTime))).thenReturn(Arrays.asList(p1, p4));
        Price price = findPriceUseCase.findByBrandIdAndProductIdAndDateTime(BRAND_ID, PRODUCT_ID, dateTime);
        assertEquals(new BigDecimal("38.95"), price.getPrice());
    }
}