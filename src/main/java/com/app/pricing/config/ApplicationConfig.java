package com.app.pricing.config;

import com.app.pricing.application.create.FindPriceUseCase;
import com.app.pricing.domain.repository.PriceRepository;
import com.app.pricing.domain.service.FindPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {
    @Bean
    public FindPriceService findPriceUseCase(@Autowired PriceRepository priceRepository) {
        return new FindPriceUseCase(priceRepository);
    }
}


