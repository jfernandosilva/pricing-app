package com.app.pricing.domain.service;

import com.app.pricing.domain.exception.PriceNotFoundException;
import com.app.pricing.domain.model.Price;

import java.time.LocalDateTime;

public interface FindPriceService {
    Price findByBrandIdAndProductIdAndDateTime(Long brandId, Long productId, LocalDateTime dateTime) throws PriceNotFoundException;
}
