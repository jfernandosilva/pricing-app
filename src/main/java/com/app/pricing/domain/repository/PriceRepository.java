package com.app.pricing.domain.repository;

import com.app.pricing.domain.model.Price;

import java.time.LocalDateTime;
import java.util.List;

public interface PriceRepository {
    List<Price> getAllByBrandIdAndProductIdAndDateTime(Long brandId, Long productId, LocalDateTime dateTime);
}