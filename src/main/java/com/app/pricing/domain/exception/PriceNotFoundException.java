package com.app.pricing.domain.exception;

public class PriceNotFoundException extends Exception {
    private String message;

    public PriceNotFoundException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
