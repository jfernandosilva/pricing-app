package com.app.pricing.infrastructure.inbound.controller;

import com.app.pricing.domain.exception.PriceNotFoundException;
import com.app.pricing.domain.model.Price;
import com.app.pricing.domain.service.FindPriceService;
import com.app.pricing.infrastructure.dto.PriceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class PricingController {
    private final FindPriceService findPriceService;

    @Autowired
    public PricingController(FindPriceService findPriceService) {
        this.findPriceService = findPriceService;
    }

    @GetMapping("/prices")
    ResponseEntity<PriceDto> findPrice(@RequestParam("date") LocalDateTime dateTime, @RequestParam("brand_id") Long brandId, @RequestParam("product_id") Long productId) throws PriceNotFoundException {
        Price price = findPriceService.findByBrandIdAndProductIdAndDateTime(brandId, productId, dateTime);
        return ResponseEntity.ok(PriceDto.mapFromPrice(price));
    }
}
