package com.app.pricing.infrastructure.inbound.enums;

public enum ErrorControllerEnum {
    SERVER_ERROR("00", "INTERNAL SERVER ERROR"),
    MISING_REQUEST_PARAMETER("01", "The request does not have the required parameters"),
    PARAMETER_TYPE_MISMATCH("02", "The request has parameters with wrong type"),
    PRICE_NOT_FOUND("03", "PRICE NOT FOUND WITH GIVEN PARAMETERS");

    ErrorControllerEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
