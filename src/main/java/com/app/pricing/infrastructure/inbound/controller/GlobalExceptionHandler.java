package com.app.pricing.infrastructure.inbound.controller;

import com.app.pricing.domain.exception.PriceNotFoundException;
import com.app.pricing.infrastructure.dto.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import static com.app.pricing.infrastructure.inbound.enums.ErrorControllerEnum.*;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(value = PriceNotFoundException.class)
    protected ResponseEntity<Object> handleThrowable(PriceNotFoundException ex) {
        ApiError apiError = new ApiError(PRICE_NOT_FOUND);
        return new ResponseEntity(apiError, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = MissingServletRequestParameterException.class)
    protected ResponseEntity<Object> handleThrowable(MissingServletRequestParameterException ex) {
        ApiError apiError = new ApiError(MISING_REQUEST_PARAMETER);
        return ResponseEntity.badRequest().body(apiError);
    }

    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    protected ResponseEntity<Object> handleThrowable(MethodArgumentTypeMismatchException ex) {
        ApiError apiError = new ApiError(PARAMETER_TYPE_MISMATCH);
        return ResponseEntity.badRequest().body(apiError);
    }
}