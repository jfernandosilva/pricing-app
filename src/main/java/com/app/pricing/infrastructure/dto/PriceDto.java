package com.app.pricing.infrastructure.dto;

import com.app.pricing.domain.model.Price;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

public class PriceDto implements Serializable {
    @JsonProperty("brand_id")
    private Long brandId;
    @JsonProperty("price_list_id")
    private Long priceListId;
    @JsonProperty("price")
    private String price;
    @JsonProperty("start_date")
    private LocalDateTime startDate;
    @JsonProperty("end_date")
    private LocalDateTime endDate;

    public PriceDto(Long brandId, Long priceListId, String price, LocalDateTime startDate, LocalDateTime endDate) {
        this.brandId = brandId;
        this.priceListId = priceListId;
        this.price = price;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getPriceListId() {
        return priceListId;
    }

    public void setPriceListId(Long priceListId) {
        this.priceListId = priceListId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public static PriceDto mapFromPrice(Price price) {
        return new PriceDto(
                price.getBrandId(),
                price.getPriceListId(),
                price.getPrice() + " " + price.getCurrency(),
                price.getStartDate(),
                price.getEndDate()
        );
    }

    @Override
    public String toString() {
        return "PriceDto{" +
                "brandId=" + brandId +
                ", priceListId=" + priceListId +
                ", price='" + price + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
