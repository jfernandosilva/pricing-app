package com.app.pricing.infrastructure.dto;

import com.app.pricing.infrastructure.inbound.enums.ErrorControllerEnum;

import java.io.Serializable;

public class ApiError implements Serializable {
    private String code;
    private String message;

    public ApiError(ErrorControllerEnum errorControllerEnum) {
        this.code = errorControllerEnum.getCode();
        this.message = errorControllerEnum.getMessage();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
