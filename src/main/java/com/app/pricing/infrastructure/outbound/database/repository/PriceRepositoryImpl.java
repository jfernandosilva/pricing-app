package com.app.pricing.infrastructure.outbound.database.repository;

import com.app.pricing.domain.model.Price;
import com.app.pricing.domain.repository.PriceRepository;
import com.app.pricing.infrastructure.outbound.database.entity.PriceEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PriceRepositoryImpl implements PriceRepository {
    private final PriceEntityJpaRepository priceEntityJpaRepository;

    @Autowired
    public PriceRepositoryImpl(PriceEntityJpaRepository priceEntityJpaRepository) {
        this.priceEntityJpaRepository = priceEntityJpaRepository;
    }

    @Override
    public List<Price> getAllByBrandIdAndProductIdAndDateTime(Long brandId, Long productId, LocalDateTime dateTime) {
        return priceEntityJpaRepository
                .getAllByBrandIdAndProductIdAndDateTime(brandId, productId, dateTime)
                .stream()
                .map(PriceEntity::toPriceInstance)
                .collect(Collectors.toList());
    }
}