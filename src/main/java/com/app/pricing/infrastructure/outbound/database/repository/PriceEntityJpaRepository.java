package com.app.pricing.infrastructure.outbound.database.repository;

import com.app.pricing.infrastructure.outbound.database.entity.PriceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface PriceEntityJpaRepository extends JpaRepository<PriceEntity, Integer> {
    @Query("SELECT price FROM PriceEntity price " +
            "WHERE price.brandId = :brandId AND price.productId = :productId " +
            "AND price.startDate <= :dateTime AND price.endDate >= :dateTime")
    List<PriceEntity> getAllByBrandIdAndProductIdAndDateTime(@Param("brandId") Long brandId,
                                                             @Param("productId") Long productId,
                                                             @Param("dateTime") LocalDateTime dateTime);
}
