package com.app.pricing.application.create;

import com.app.pricing.domain.exception.PriceNotFoundException;
import com.app.pricing.domain.model.Price;
import com.app.pricing.domain.repository.PriceRepository;
import com.app.pricing.domain.service.FindPriceService;

import java.time.LocalDateTime;
import java.util.Comparator;

public class FindPriceUseCase implements FindPriceService {

    private final PriceRepository priceRepository;

    public FindPriceUseCase(PriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }


    @Override
    public Price findByBrandIdAndProductIdAndDateTime(Long brandId, Long productId, LocalDateTime dateTime) throws PriceNotFoundException {
        return priceRepository
                .getAllByBrandIdAndProductIdAndDateTime(brandId, productId, dateTime)
                .stream()
                .max(Comparator.comparing(Price::getPriority))
                .orElseThrow(() -> new PriceNotFoundException("Price not found with date " + dateTime.toString()));
    }
}
