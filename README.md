# Pricing app

To run the application is necessary you have Java 17 installed and then enter to a terminal in the project root path and execute the next command:

```sh
./mvnw spring-boot:run
```

**To execute the tests use next command:**
```sh
./mvnw test
```

** The app has an endpoint with path /prices**
You must send the next path parameters:
* **date**: It is the date of price consultation in format **yyyy-MM-dd HH:mm:ss**
* **brand_id**: The product's brand
* **product_id**: The product's id


**Endpoint execution examples using curl:**
* date: 2020-06-14 10:00:00 - brand_id: 1 - product_id: 35455
```sh
curl --location 'localhost:8080/prices?date=2020-06-14%2010%3A00%3A00&brand_id=1&product_id=35455'
```

* date: 2020-06-14 16:00:00 - brand_id: 1 - product_id: 35455
```sh
curl --location 'localhost:8080/prices?date=2020-06-14%2016%3A00%3A00&brand_id=1&product_id=35455'
```

* date: 2020-06-14 21:00:00 - brand_id: 1 - product_id: 35455
```sh
curl --location 'localhost:8080/prices?date=2020-06-14%2021%3A00%3A00&brand_id=1&product_id=35455'
```

* date: 2020-06-15 10:00:00 - brand_id: 1 - product_id: 35455
```sh
curl --location 'localhost:8080/prices?date=2020-06-15%2010%3A00%3A00&brand_id=1&product_id=35455'
```

* date: 2020-06-16 21:00:00 - brand_id: 1 - product_id: 35455
```sh
curl --location 'localhost:8080/prices?date=2020-06-16%2021%3A00%3A00&brand_id=1&product_id=35455'
```